/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package babysteptdd;

/**
 *
 * @author samer
 */
public class BabyStepTDD {

    static String diamond(String a) {
        int number = a.charAt(0) - 'A';
        if(number == 0) {
            return helper(number, number);
        } else {
            return helper(number-1,number)+"\nB B\n"+helper(number-1,number);
        }
    }
    
    private static String helper(int currentLevel, int startLevel) {
        StringBuffer buffer = new StringBuffer(spaces(startLevel * 2 + 1));
        char letter = (char)('A' + currentLevel);
        int offset = startLevel - currentLevel;
        buffer.setCharAt(offset, letter);
        buffer.setCharAt(buffer.length() - offset - 1, letter);
        return buffer.toString();
    }

    private static String spaces(int spacesCount) {
        return (spacesCount == 0) ? "" : String.format("%" + (spacesCount) + "s", "");
    }    
}
